[![CI Status](https://magnum-ci.com/status/2c2313c29387e744fb7c1c438443b20c.png)](https://magnum-ci.com/public/3a5d88927a03611f50d6/builds)

[![NPM](https://nodei.co/npm/nlearn.png?downloads=true&downloadRank=true&stars=true)](https://nodei.co/npm/nlearn/)

# Advantages #

* Pure JS implementation
* .d.ts file is provided for implementing in your TypeScript project
* Internal computations implemented with Float64Array for performance

# Alternative Libraries #

* ml-js: Implements more learning models currently. However, a number of native dependencies are required
* node-svm: Implements SVM classifier only, while nlearn is designed to provide a consistent interface for multiple classifiers
* node-fann: Again, a single classifier type. Also depends on native libraries with special installation requirements.
* machine_learning: Implements many supervised learning models. You might want to choose this for now until this library is robust enough.

# Basic Usage #

    var nlearn = require("nlearn");
    var Example = nlearn.Example;
    var BinaryLabel = nlearn.BinaryLabel;

    var data = [
                new Example(new Float64Array([1,2,3]), BinaryLabel.UP),
                new Example(new Float64Array([1,3,7]), BinaryLabel.DOWN),
                new Example(new Float64Array([4,5,8]), BinaryLabel.DOWN)
                ];

    var kernel = new nlearn.RbfKernel(10);

    var classifier = nlearn.SvmFactory.create(kernel);
    classifier.fit(data);
    var label = classifier.classify([1,2,3]); //BinaryLabel.UP


# Choosing a Best Fit Classifier #

Some classifiers will fit your data better than others. You can use an Optimizer to run analysis on a set of classifier parameters and
choose the best fit. Note that using the optimizer will partition your training data set, so the effective training set is cut in half.


    var tolerance = new nlearn.GeometricRangeIterator(new nlearn.ImmutableRange(1e-10, 1e-3), 10);
    var alphaTolerance = new nlearn.GeometricRangeIterator(new nlearn.ImmutableRange(1e-10, 1e-3), 10);
    var C = new nlearn.GeometricRangeIterator(new nlearn.ImmutableRange(0.1, 1000), 10);
    var kernels = [new nlearn.LinearKernel(), new nlearn.RbfKernel(1), new nlearn.RbfKernel(10)];
    var kernelIterator = new nlearn.ArrayIterator(kernels);
    var optimizer = new nlearn.SvmOptimizer(kernelIterator, tolerance, alphaTolerance, C);

    var data = [];
    data.push(new Example(new Float64Array([4,4,6,8]), BinaryLabel.UP));
    data.push(new Example(new Float64Array([5,4,6,8]), BinaryLabel.UP));
    data.push(new Example(new Float64Array([6,4,6,8]), BinaryLabel.UP));
    data.push(new Example(new Float64Array([1,4,6,8]), BinaryLabel.DOWN));
    data.push(new Example(new Float64Array([2,4,6,8]), BinaryLabel.DOWN));
    data.push(new Example(new Float64Array([3,4,6,8]), BinaryLabel.DOWN));
    var classifier = optimizer.optimize(data);
