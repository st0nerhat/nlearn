"use strict";
import {Iterator} from "./datatypes";

export class ArrayIterator<T> implements Iterator<T> {
	private idx:number = -1;

	public get value():T {
		if (this.idx == -1 || this.idx > this.array.length - 1)
			return null;

		return this.array[this.idx];
	}

	constructor(private array:Array<T>) {
	}

	public reset():void {
		this.idx = -1;
	}

	public next():T {
		if (this.idx < this.array.length)
			this.idx++;

		var result = this.array[this.idx];

		if (result === undefined || result === null)
			return null;
		else
			return result;
	}
}