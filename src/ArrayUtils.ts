/**
 * In-place Fisher-Yates shuffle
 * http://bost.ocks.org/mike/shuffle/
 */

"use strict";

export function shuffle<T>(array:Array<T>):Array<T> {
	var m = array.length, t:T, i:number;

	// While there remain elements to shuffle…
	while (m) {

		// Pick a remaining element…
		i = Math.floor(Math.random() * m--);

		// And swap it with the current element.
		t = array[m];
		array[m] = array[i];
		array[i] = t;
	}

	return array;
}