"use strict";

var aliasToClass:any = {};

export function add(alias:string, cls:any):void {
	aliasToClass[alias] = cls;
}

export function get(alias:string):any {
	return aliasToClass[alias];
}