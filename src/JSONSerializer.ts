import * as ClassRegistry from "./ClassRegistry";

"use strict";
export function toJSON(obj:any):string {
	return JSON.stringify(obj);
}

export function fromJSON(json:string):any {
	return JSON.parse(json, (name, val) => {
		if (val && val._type) {
			var classObj:any = ClassRegistry.get(val._type);
			return classObj.fromJSON(val);
		} else {
			return val;
		}
	});
}

