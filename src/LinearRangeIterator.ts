"use strict";
import * as assert from "assert";
import {ImmutableRange, Iterator} from "./datatypes";

/**
 * Iterate over a real number line segment at a fixed step
 */
export class LinearRangeIterator implements Iterator<number> {
	private _value:number = null;

	public get value():number {
		return this._value;
	}

	constructor(private _range:ImmutableRange, private _stepSize:number) {
		assert.ok(_stepSize > 0);
	}

	public reset():void {
		this._value = null;
	}

	public next():number {
		if (this._value === null)
			this._value = this._range.min;
		else if (this._value == this._range.max) {
			this._value = null;
		}
		else {
			this._value += this._stepSize;

			if (this._value > this._range.max)
				this._value = this._range.max;
		}

		return this._value;
	}
}
