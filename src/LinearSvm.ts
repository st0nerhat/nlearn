"use strict";
import * as ClassRegistry from "./ClassRegistry";
import {Svm} from "./Svm";
import {LinearKernel} from "./kernels";
import {Example} from "./datatypes";

/**
 * An optimized linear kernel SVM implementation.
 */
export class LinearSvm extends Svm {
	private weights:Float64Array;

	constructor(tolerance:number = 1e-4, alphaTolerance:number = 1e-7,  C:number = 1.0) {
		super(new LinearKernel(), tolerance, alphaTolerance, C);
	}

	protected computeMargin(values:Float64Array, i1?:number):number {
		if (this.weights) {
			var f:number = 0.0;

			for (var i:number = 0; i < this.inputCardinality; i++) {
				f += values[i] * this.weights[i];
			}

			f -= this.bias;

			return f;
		} else {
			return super.computeMargin(values, i1);
		}
	}

	private computeWeights():void {
		this.weights = new Float64Array(this.inputCardinality);
		var D = this.inputCardinality;
		var N = this.supportVectors.length;
		var supportVectors = this.supportVectors;

		for (var i:number = 0; i < D; i++) {
			var w:number = 0.0;

			for (var j:number = 0; j < N; j++) {
				var example = supportVectors[j];

				w += example.alpha * example.label * example.features[i];
			}

			this.weights[i] = w;
		}
	}

	public fit(data:Array<Example>):Q.Promise<void> {
		var p = super.fit(data);
		this.computeWeights();
		this.supportVectors = null; //Throw out the data. We can just use the weights.
		return p;
	}

	public toJSON():any {
		var data:any = super.toJSON();
		data._type = "nlearn.LinearSvm";
		data.weights = this.weights;
		return data;
	}

	public static fromJSON(data:any, svm?:LinearSvm):Svm {
		if (!svm)
			svm = new LinearSvm(data.tolerance, data.alphaTolerance, data.C);
		svm.weights = new Float64Array(data.weights);
		return Svm.fromJSON(data, svm);
	}
}

ClassRegistry.add("nlearn.LinearSvm", LinearSvm);