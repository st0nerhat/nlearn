"use strict";
import * as assert from "assert";
import {shuffle} from "./ArrayUtils";
import {Example} from "./datatypes";

export class PartitionedDataSet {
	private _trainingSet:Array<Example>;
	private _evaluationSet:Array<Example>;

	public get trainingSet():Array<Example> {
		return this._trainingSet;
	}

	public get evaluationSet():Array<Example> {
		return this._evaluationSet;
	}

	constructor(data:Array<Example>, t:number = 0.5) {
		assert.ok(t > 0 && t < 1);
		//Shuffle the dataset to remove any bias in the order of training features
		shuffle(data);
		var pos:number = Math.round(data.length * t);
		//Partition the dataset
		this._trainingSet = data.slice(0, pos);
		this._evaluationSet = data.slice(pos);
		assert.equal(this._trainingSet.length + this._evaluationSet.length, data.length);
	}
}
