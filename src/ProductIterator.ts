"use strict";
import {Iterator} from "./datatypes.ts";

/**
 * Iterate over the product of a set of sets
 */
export class ProductIterator implements Iterator<any> {

    private _value:any = null;

    public get value():any {
        return this._value;
    }

    constructor(private ranges:any) {
    }

    public reset():void {
        this._value = null;
        for (var k in this.ranges) {
            this.ranges[k].reset();
        }
    }

    public next():any {
        var result:any = {};

        var increment:boolean = true;
        var overflow:boolean = false;

        for (var k in this.ranges) {
            var entry:Iterator<any> = this.ranges[k];
            var res:any;
            //We only want to detect if the last set overflows, so reset here
            overflow = false;

            //Handle first time through where iterator needs to be inited
            if (increment || entry.value === null)
                res = entry.next();
            else
                res = entry.value; //Don't change values because a previous entry in the set was already permuted

            if (res === null) {
                //We reached the end of the iterator, so start over from the beginning
                entry.reset();
                res = entry.next();
                result[k] = res;
                overflow = true;
            } else {
                //We permuted, so don't change any other values in the result set
                result[k] = res;
                increment = false;
            }
        }

        //If we looped back over on the last set, then we are finished
        if (overflow) {
            result = null;
        }

        this._value = result;

        return result;
    }
}