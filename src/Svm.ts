"use strict";
import * as assert from "assert";
import * as Q from "q";
import * as ClassRegistry from "./ClassRegistry";
import {Example, Classifier, TrainingMetrics, CacheMetrics, BinaryLabel} from "./datatypes";
import {Kernel} from "./kernels";

export class SvmExample extends Example {
	constructor(data:Example, public alpha:number = 0.0, public error:number = 0.0, public bounded:boolean = false) {
		super(data.features, data.label);
	}

	public static fromJSON(data:any):SvmExample {
		return new SvmExample(new Example(new Float64Array(data.features), data.label), data.alpha);
	}

	public toJSON():any {
		//Don't save the error value
		return {"_type": "nlearn.SvmExample", "features": this.features, "label": this.label, "alpha": this.alpha};
	}
}

ClassRegistry.add("nlearn.SvmExample", SvmExample);

export class Svm implements Classifier {
	private allExamples:Array<SvmExample>;
	protected bias:number;
	protected kernel:Kernel;
	protected supportVectors:Array<SvmExample>;
	private kernelCache:KernelCache;
	private _cardinality:number;
	private _trained:boolean = false;
	private _trainingMetrics:TrainingMetrics;
	private _errorCacheMetrics:CacheMetrics;

	public get errorCacheMetrics():CacheMetrics {
		return this._errorCacheMetrics;
	}

	public get trainingMetrics():TrainingMetrics {
		return this._trainingMetrics;
	}

	public get kernelCacheMetrics():CacheMetrics {
		return this.kernelCache.metrics;
	}

	public get trained():boolean {
		return this._trained;
	}

	public get inputCardinality():number {
		return this._cardinality;
	}

	public get outputCardinality():number {
		return 1; //Svms only support one output
	}

	constructor(kernel:Kernel, private tolerance:number = 1e-4, private alphaTolerance:number = 1e-7, private C:number = 1.0) {
		this.kernel = kernel;
		this.kernelCache = new NoopKernelCache(kernel);
	}

	public classify(values:Float64Array):number {
		assert.equal(this.inputCardinality, values.length);
		return this.computeMargin(values) > 0.0 ? BinaryLabel.UP : BinaryLabel.DOWN;
	}


	public evaluate(data:Array<Example>):number {
		var correct:number = 0;

		for (var i:number = 0; i < data.length; i++) {
			var example = data[i];
			if(this.classify(example.features) == example.label)
				correct++;
		}

		return correct / data.length;
	}

	protected computeMargin(values:Float64Array, i1?:number):number {
		var f:number = 0.0;
		var examples:Array<SvmExample>;

		if (this.supportVectors) {
			examples = this.supportVectors;
		} else {
			examples = this.allExamples;
		}

		var N = examples.length;

		//TODO: Handle sparse samples
		//see http://cs229.stanford.edu/materials/smo.pdf (fig 2)
		for (var i:number = 0; i < N; i++) {
			var example:SvmExample = examples[i];

			if (example.alpha > 0.0)
				f += example.label * example.alpha * this.kernelCache.compute(values, example.features, i1, i);
		}

		f -= this.bias;
		return f;
	}

	/**
	 * Train using Sequential Minimal Optimization
	 */
	public fit(data:Array<Example>):Q.Promise<void> {
		var d:Q.Deferred<void> = Q.defer<void>();

		//Clear kernel state
		this.kernelCache = new KernelCache(this.kernel, data.length);
		this._errorCacheMetrics = new CacheMetrics();

		//Convert to SvmExample
		var allExamples:Array<SvmExample>;
		this.allExamples = allExamples = data.map((a:Example):SvmExample => {
			return new SvmExample(a);
		});

		//Calculate cardinality from first data point
		this._cardinality = data[0].features.length;

		this.bias = 0.0;

		var iter:number = 0;
		var alphaEps:number = this.alphaTolerance;
		var C:number = this.C;
		var N:number = allExamples.length;
		var examineAll:boolean = true;
		var alphaChanged:boolean = false;

		//If the lagrange multipliers have not changed for an iteration and then again after a full sweep
		//we have converged.
		while (alphaChanged || examineAll) {
			alphaChanged = false;
			iter++;

			//Update the alpha value for each data point
			for (var i:number = 0; i < N; i++) {
				var example1:SvmExample = allExamples[i];

				if (examineAll) {
					alphaChanged = alphaChanged || this.examineSample(i, example1);
				} else {
					if (example1.alpha !== 0.0 && example1.alpha != C) {
						alphaChanged = alphaChanged || this.examineSample(i, example1);
					}
				}
			}

			examineAll = examineAll ? false : !alphaChanged;
		}

		//Reduce the dataset to just the necessary data
		this.supportVectors = allExamples.filter((example:SvmExample) => {
			return example.alpha > alphaEps;
		});

		this._trained = true;
		this._trainingMetrics = new TrainingMetrics(iter, this.allExamples.length, this.supportVectors.length);
		this.allExamples = null;
		this.kernelCache.reset();

		d.resolve(null); //Resolve immediately for now
		return d.promise;
	}

	protected examineSample(i1:number, example1:SvmExample):boolean {
		var C:number = this.C;
		var eps:number = this.tolerance;
		var alph1:number = example1.alpha;
		var x1:Float64Array = example1.features;
		var y1:number = example1.label;

		//Compute the error between the correct class and the computed class (-2 <= e2 <= 2)
		var e1 = this.computeError(example1, i1);
		//Add direction back into the error value
		var r2 = y1 * e1;

		//Only examine if there is an error and alpha is in the box
		if ((r2 < -eps && alph1 < C)
			|| (r2 > eps && alph1 > 0.0)) {

			var example2:SvmExample = null;
			var allExamples = this.allExamples;
			var N:number = allExamples.length;
			var i2:number = -1;

			//Choice heuristic #1: Choose the example with the maximum error difference
			for (var tmax:number = 0, k:number = 0; k < N; k++) {
				var candidate = allExamples[k];

				if (!candidate.bounded)
					continue;

				var e2 = this.computeError(candidate, k);
				var eDelta = Math.abs(e1 - e2);
				if (eDelta > tmax) {
					tmax = eDelta;
					i2 = k;
					example2 = candidate;
				}
			}

			if (example2 !== null && this.takeStep(i1, i2, example1, example2, x1, y1, e1, alph1)) {
				return true;
			}

			//Choice heuristic #2: Iterate through all examples where 0 < alpha < C
			for (var j:number = Svm.randomInt(0, N), k:number = j; k < N + j; k++) {
				i2 = k % N;
				example2 = allExamples[i2];

				if (example2.bounded && this.takeStep(i1, i2, example1, example2, x1, y1, e1, alph1)) {
					return true;
				}
			}

			//Choice heuristic #3: Iterate through all unbounded examples starting at a random point
			for (var j:number = Svm.randomInt(0, N), k:number = j; k < N + j; k++) {
				i2 = k % N;
				example2 = allExamples[i2];

				if(!example2.bounded && this.takeStep(i1, i2, example1, example2, x1, y1, e1, alph1))
					return true;
			}
		}

		return false;
	}

	protected takeStep(i1:number, i2:number, example1:SvmExample, example2:SvmExample, x1:Float64Array, y1:number, e1:number, alph1:number):boolean {
		//Don't compare the vector to itself
		if (example1 == example2)
			return false;

		var alphaEps:number = this.alphaTolerance;
		var eps:number = this.tolerance;
		var b:number = this.bias;
		var y2:number = example2.label;
		var alph2:number = example2.alpha;
		var x2:Float64Array = example2.features;
		var C:number = this.C;
		var N:number = this.allExamples.length;
		var allExamples = this.allExamples;
		var kernelCache = this.kernelCache;

		//Compute the error for the second point
		var e2:number = this.computeError(example2, i2);

		//Find lagrange multipliers that solve initial constraint -C <= L <= 0 and 0 <= H <= C
		//See http://research.microsoft.com/pubs/68391/smo-book.pdf (fig 12.3 and 12.4)
		//See http://www.cs.iastate.edu/~honavar/smo-svm.pdf (fig 22a)
		var L:number, H:number, gamma:number;

		if (y1 == y2) {
			gamma = alph1 + alph2;
			if (gamma > C) {
				L = gamma - C;
				H = C;
			} else {
				L = 0.0;
				H = gamma;
			}
		} else {
			gamma = alph1 - alph2;
			if (gamma > 0.0) {
				L = 0.0;
				H = C - gamma;
			} else {
				L = -gamma;
				H = C;
			}
		}

		//If the difference between the multipliers is very small, this support vector is already minimized
		//with respect to the second point
		if (Math.abs(L - H) < eps) return false;

		//Compute a couple of kernel features we'll need later
		var k11 = kernelCache.compute(x1, x1, i1, i1);
		var k12 = kernelCache.compute(x1, x2, i1, i2);
		var k22 = kernelCache.compute(x2, x2, i2, i2);

		// Compute the second derivative of the objective function to determine whether we are instantaneously sloping up or down
		var eta = 2.0 * k12 - k11 - k22;
		var s = y1 * y2;
		var a2:number;
		var a1:number;

		if (eta < 0.0) {
			//Compute unconstrained maximum
			//See http://research.microsoft.com/pubs/68391/smo-book.pdf (fig 12.6)
			a2 = alph2 - y2 * (e1 - e2) / eta;

			//Clamp alpha so that L <= alpha <= H
			//See http://research.microsoft.com/pubs/68391/smo-book.pdf (fig 12.7)
			if (a2 < L) a2 = L;
			else if (a2 > H) a2 = H;
		} else {
			//Handle case where eta >= 0
			var c1 = eta / 2.0;
			var c2 = y2 * (e1 - e2) - eta * alph2;
			var Lobj = c1 * L * L + c2 * L;
			var Hobj = c1 * H * H + c2 * H;

			if (Lobj > (Hobj + eps)) {
				a2 = L;
			} else if (Lobj < (Hobj - eps)) {
				a2 = H;
			} else {
				a2 = alph2;
			}
		}

		//Clamp 0 <= a2 <= C in case of loss of precision
		if (a2 < alphaEps) {
			a2 = 0.0;
		} else if (a2 > C - alphaEps) {
			a2 = C;
		}

		var a2Delta = a2 - alph2;

		//If there is minimal difference between the new and old alpha, move on
		if (Math.abs(a2Delta) < eps * (a2 + alph2 + eps))
			return false;

		//Compute the new alpha for the first data point
		a1 = alph1 + s * (alph2 - a2);

		//Clamp 0 <= a1 <= C in case of loss of precision
		if (a1 < alphaEps) {
			a1 = 0.0;
		} else if (a1 > C - alphaEps) {
			a1 = C;
		}

		example1.bounded = a1 > 0.0 && a1 < C;
		example2.bounded = a2 > 0.0  && a2 < C;

		//Update the bias which forces 0 <= alpha <= C
		//see http://cs229.stanford.edu/materials/smo.pdf (3.3)
		//see http://www.cs.iastate.edu/~honavar/smo-svm.pdf(fig 23a)
		var a1Delta = a1 - alph1;

		if (example1.bounded) {
			b = b + e1 + y1 * a1Delta * k11 + y2 * a2Delta * k12;
		} else {
			if (example2.bounded) {
				b = b + e2 + y1 * a1Delta * k12 + y2 * a2Delta * k22;
			} else {
				b = (b + e1 + y1 * a1Delta * k11 + y2 * a2Delta * k12 + b + e2 + y1 * a1Delta * k12 + y2 * a2Delta * k22) * 0.5;
			}
		}

		var deltaB:number = b - this.bias;

		//Save the new bias
		this.bias = b;

		//Save the new lagrange multipliers
		example1.alpha = a1;
		example2.alpha = a2;


		//Update error cache
		var t1:number = y1 *  a1Delta;
		var t2:number = y2 * a2Delta;

		for (var i:number = 0; i < N; i++) {
			var dp3:SvmExample = allExamples[i];

			if (!dp3.bounded || dp3 === example1 || dp3 === example2)
				continue;

			var x3:Float64Array = dp3.features;
			this.errorCacheMetrics.sets++;
			dp3.error += t1 * kernelCache.compute(x1, x3, i1, i) + t2 * kernelCache.compute(x2, x3, i2, i) - deltaB;
		}

		example1.error = 0.0;
		example2.error = 0.0;

		return true;
	}

	private computeError(d:SvmExample, i1:number):number {
		if(d.bounded) {
			this._errorCacheMetrics.hits++;
			return d.error;
		} else {
			this._errorCacheMetrics.misses++;
			return this.computeMargin(d.features, i1) - d.label;
		}
	}

	/**
	 * A number between min and max - 1
	 * @param min
	 * @param max
	 * @returns {number}
	 */
	private static randomInt(min:number, max:number):number {
		return Math.floor(Math.random() * (max - min) + min);
	}

	public toJSON():any {
		return {
			"_type":"nlearn.Svm",
			"inputCardinality":this._cardinality,
			"C":this.C,
			"tolerance":this.tolerance,
			"alphaTolerance":this.alphaTolerance,
			"kernel":this.kernel,
			"supportVectors":this.supportVectors,
			"trained":this._trained,
			"bias":this.bias
		};
	}

	public static fromJSON(data:any, svm?:Svm):Svm {
		if(!svm)
			svm = new Svm(data.kernel, data.tolerance, data.alphaTolerance, data.C);
		svm.supportVectors = data.supportVectors;
		svm.bias = data.bias;
		svm._cardinality = data.inputCardinality;
		svm._trained = data.trained;
		return svm;
	}
}


/**
 * Implements a full cache of all kernel computations. The cache is lazily filled.
 */
class KernelCache  {
	//TODO: Implement fixed-size LRU cache
	private _metrics:CacheMetrics = new CacheMetrics();
	private cache:Float64Array;

	public get metrics():CacheMetrics {
		return this._metrics
	}

	constructor(protected kernel:Kernel, private dataSize:number) {
		this.reset();
	}

	public compute(x:Float64Array, y:Float64Array, i1?:number, i2?:number):number {
		var canCache:boolean = false;
		var hash:number;

		//We can only cache it if the samples are both from the training set
		if (i1 !== undefined && i2 !== undefined) {
			hash = this.dataSize * (i1 % this.dataSize) + i2;
			var lookup1:number = this.cache[hash];
			if (lookup1 != Number.POSITIVE_INFINITY) {
				this._metrics.hits++;
				return lookup1;
			}

			//Only track misses on cacheable entries
			this._metrics.misses++;
			canCache = true;
		}

		var result:number = this.kernel.compute(x, y);

		if (canCache) {
			this._metrics.sets++;
			this.cache[hash] = result;
		}

		return result;
	}

	public reset():void {
		var N:number = this.dataSize * this.dataSize;

		this.cache = new Float64Array(N);
		for (var i:number = 0; i < N; i++) {
			this.cache[i] = Number.POSITIVE_INFINITY;
		}
	}
}

class NoopKernelCache extends KernelCache {
	constructor(kernel:Kernel) {
		super(kernel, 1);
	}

	public compute(x:Float64Array, y:Float64Array, i1?:number, i2?:number):number {
		return this.kernel.compute(x, y);
	}
}

ClassRegistry.add("nlearn.Svm", Svm);
