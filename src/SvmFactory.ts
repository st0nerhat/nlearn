
"use strict";
import {Kernel, LinearKernel} from "./kernels";
import {Svm} from "./Svm";
import {LinearSvm} from "./LinearSvm";

export function create(kernel:Kernel, tolerance?:number, alphaTolerance?:number,  C?:number):Svm {
	if (kernel instanceof LinearKernel) {
		return new LinearSvm(tolerance, alphaTolerance, C);
	} else {
		return new Svm(kernel, tolerance, alphaTolerance, C);
	}
}