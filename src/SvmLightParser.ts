/// <reference path="../types/types.d.ts" />

"use strict";
import * as stream from "stream";
import * as readline from "readline";
import * as Q from "q";
import {ExampleParser, Example} from "./datatypes";

export class SvmLightParser implements ExampleParser {
	constructor(private inStream:stream.Readable, private featureCount:number) {

	}

	public parse():Q.Promise<Array<Example>> {
		var D:Q.Deferred<Array<Example>> = Q.defer<Array<Example>>();
		var dataSet:Array<Example> = new Array<Example>();
		var outStream:stream.Writable = new stream.Writable();

		var rl:readline.ReadLine = readline.createInterface({input:this.inStream, output:outStream});

		rl.on("line", (line:string):void => {
			if (line.substr(0, 1) === "#")
				return;

			var entries:Array<string> = line.split(" ");
			entries[0] = entries[0].replace("+", "");
			var label:number = parseFloat(entries[0]);
			var values:Float64Array = new Float64Array(this.featureCount);

			for (var i:number = entries.length - 1; i > 0; i-- ) {
				var featSplit:Array<string> = entries[i].split(":");
				var featIndex = parseInt(featSplit[0], 10);

				values[featIndex] = parseFloat(featSplit[1]);
			}

			var dp:Example = new Example(values, label);
			dataSet.push(dp);
		});

		rl.on("close", ():void => {
			D.resolve(dataSet);
		});

		return D.promise;
	}
}
