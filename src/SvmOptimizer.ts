"use strict";
import {Optimizer, Classifier, Example, ClassifierMetrics, OptimizerResult, Iterator} from "./datatypes";
import {ProductIterator} from "./ProductIterator";
import {Kernel} from "./kernels";
import {PartitionedDataSet} from "./PartitionedDataSet";
import {Svm} from "./Svm";
import * as SvmFactory from "./SvmFactory";

export class SvmOptimizer implements Optimizer {
	private iterator:ProductIterator;

	constructor(kernel:Iterator<Kernel>, tolerance:Iterator<number>, alphaTolerance:Iterator<number>, C:Iterator<number>) {
		var iterSet:any = {};
		iterSet.kernel = kernel;
		iterSet.tolerance = tolerance;
		iterSet.alphaTolerance = alphaTolerance;
		iterSet.C = C;
		this.iterator = new ProductIterator(iterSet);
	}

	//TODO: Optimize accuracy, time, and space
	public optimize(data:Array<Example>):OptimizerResult {
		var s:any;
		//TODO: Use a variable partition of the dataset
		var dataSet:PartitionedDataSet = new PartitionedDataSet(data);
		var bestScore:number = -1;
		var bestClassifier:Classifier = null;
		var accuracy:number;

		//TODO: Use a more efficient algorithm than an exhaustive search
		while (s = this.iterator.next()) {
			//Choose the best Svm implementation for the given kernel
			var classifier:Svm = SvmFactory.create(s.kernel, s.tolerance, s.alphaTolerance, s.C);
			classifier.fit(dataSet.trainingSet);
			accuracy = 0;

			//Score classifier against evaluation set
			accuracy = classifier.evaluate(dataSet.evaluationSet);


			//TODO: Early out if the best score is found
			if (accuracy > bestScore) {
				bestScore = accuracy;
				bestClassifier = classifier;
			}
		}

		return new OptimizerResult(bestClassifier, new ClassifierMetrics(bestScore / dataSet.evaluationSet.length, classifier.trainingMetrics.finalSize));
	}
}
