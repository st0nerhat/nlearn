/// <reference path="../types/types.d.ts" />

"use strict";
import * as assert from "assert";
import * as ClassRegistry from "./ClassRegistry";

export interface Classifier {
	trained:boolean;
	inputCardinality:number;
	outputCardinality:number;

	classify(values:Float64Array):number;
	evaluate(data:Array<Example>):number;
	fit(data:Array<Example>):Q.Promise<void>;
}

export interface Optimizer {
	optimize(data:Array<Example>):OptimizerResult;
}

export enum BinaryLabel {
	DOWN = -1,
	UP = 1
}

export class TrainingMetrics {
	constructor(public iterations:number, public initialSize:number, public finalSize:number) {
	}
}

export class CacheMetrics {
	public get gets():number {
		return this.hits + this.misses;
	}

	public get hitRatio():number {
		return this.hits / this.gets;
	}

	constructor(public hits:number = 0, public misses:number = 0, public sets:number = 0) {
	}
}

export class Example {
	constructor(public features:Float64Array, public label:number) {
	}

	public static fromJSON(data:any):Example {
		return new Example(new Float64Array(data.features), data.label);
	}

	public toJSON():any {
		//Convert Float64Array to untyped array for proper serialization
		var featuresArray:Array<number> = [];

		for (var i:number = 0; i < this.features.length; i++) {
			featuresArray[i] = this.features[i];
		}

		return {"_type": "nlearn.Example", "features": featuresArray, "label": this.label};
	}
}


ClassRegistry.add("nlearn.Example", Example);


/**
 * An immutable real number line segment
 */
export class ImmutableRange {
	public get min():number {
		return this._min;
	}

	public get max():number {
		return this._max;
	}

	public get length():number {
		return this._max - this._min;
	}

	constructor(private _min:number, private _max:number) {
		assert.ok(_min <= _max);
	}
}

export interface Iterator<T> {
	next():T;
	reset():void;
	value:T;
}

export class ClassifierMetrics {
	constructor(public accuracy:number, public size:number) {
	}
}

export class OptimizerResult {
	constructor(public classifier:Classifier, public metrics:ClassifierMetrics) {
	}
}

export interface ExampleParser {
	parse():Q.Promise<Array<Example>>;
}
