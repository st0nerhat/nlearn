"use strict";
import * as ClassRegistry from "./ClassRegistry";

export interface Kernel {
	compute(x:Float64Array, y:Float64Array):number;
}

export class LinearKernel implements Kernel {
	public compute(x:Float64Array, y:Float64Array):number {
		var s:number = 0;
		for (var i:number = 0; i < x.length; i++) {
			s += x[i] * y[i];
		}

		return s;
	}

	public static fromJSON(data:any):LinearKernel {
		return new LinearKernel();
	}

	public toJSON(kernel:LinearKernel):any {
		return {"_type":"nlearn.LinearKernel"};
	}
}

export class RbfKernel implements Kernel {
	private twoSigmaSquared:number;

	constructor(private sigma:number) {
		this.twoSigmaSquared = 2.0 * sigma * sigma;
	}

	public compute(x:Float64Array, y:Float64Array):number {
		var s:number = 0;

		for (var i:number = 0; i < x.length; i++) {
			s += (x[i] - y[i]) * (x[i] - y[i]);
		}

		return Math.exp(-s / this.twoSigmaSquared);
	}

	public static fromJSON(data:any):RbfKernel {
		return new RbfKernel(data.sigma);
	}

	public toJSON(kernel:RbfKernel):any {
		return {"_type":"nlearn.RbfKernel", "sigma": kernel.sigma};
	}
}

export class PolynomialKernel implements Kernel {
	constructor(private degree:number, private c:number) {

	}

	public compute(x:Float64Array, y:Float64Array):number {
		var s:number = this.c;
		for (var i:number = 0; i < x.length; i++) {
			s+= x[i] * y[i];
		}

		return Math.pow(s, this.degree);
	}

	public static fromJSON(data:any):PolynomialKernel {
		return new PolynomialKernel(data.degree, data.c);
	}

	public toJSON():any {
		return {"_type":"nlearn.PolynomialKernel", "degree": this.degree, "c": this.c};
	}
}

ClassRegistry.add("nlearn.LinearKernel", LinearKernel);
ClassRegistry.add("nlearn.RbfKernel", RbfKernel);
ClassRegistry.add("nlearn.PolynomialKernel", PolynomialKernel);