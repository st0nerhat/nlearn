/// <reference path="../../types/types.d.ts" />
import {ArrayIterator} from "../ArrayIterator";
import * as chai from "chai";

describe("ArrayIterator", () => {
	it("can iterate forward", () => {

		var array = [1, 2, 3, 4, 5];
		var iter = new ArrayIterator(array);
		chai.expect(iter.value).to.equal(null);
		chai.expect(iter.next()).to.equal(1);
		chai.expect(iter.next()).to.equal(2);
		chai.expect(iter.value).to.equal(2);
		chai.expect(iter.next()).to.equal(3);
		chai.expect(iter.next()).to.equal(4);
		chai.expect(iter.next()).to.equal(5);
		chai.expect(iter.next()).to.equal(null);
		chai.expect(iter.next()).to.equal(null);
	});

});