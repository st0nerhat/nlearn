/// <reference path="../../types/types.d.ts" />


import * as chai from "chai";
import {ImmutableRange} from "../datatypes";
import {GeometricRangeIterator} from "../GeometricRangeIterator";

describe("GeometricRangeIterator", () => {
	it("can iterate forward", () => {
		var iter = new GeometricRangeIterator(new ImmutableRange(1, 16), 2);
		chai.expect(iter.value).to.equal(null);
		chai.expect(iter.next()).to.equal(1);
		chai.expect(iter.next()).to.equal(2);
		chai.expect(iter.value).to.equal(2);
		chai.expect(iter.next()).to.equal(4);
		chai.expect(iter.next()).to.equal(8);
		chai.expect(iter.next()).to.equal(16);
		chai.expect(iter.next()).to.equal(null);
	});

});