/// <reference path="../../types/types.d.ts" />

import * as chai from "chai";
import {ImmutableRange} from "../datatypes";
import {LinearRangeIterator} from "../LinearRangeIterator";

describe("LinearRangeIterator", () => {
    it("can iterate forward", () => {
        var iter = new LinearRangeIterator(new ImmutableRange(0, 10), 2);
        chai.expect(iter.value).to.equal(null);
        chai.expect(iter.next()).to.equal(0);
        chai.expect(iter.next()).to.equal(2);
        chai.expect(iter.value).to.equal(2);
        chai.expect(iter.next()).to.equal(4);
        chai.expect(iter.next()).to.equal(6);
        chai.expect(iter.next()).to.equal(8);
        chai.expect(iter.next()).to.equal(10);
        chai.expect(iter.next()).to.equal(null);
    });

});