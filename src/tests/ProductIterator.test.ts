/// <reference path="../../types/types.d.ts" />

import * as chai from "chai";
import {ImmutableRange} from "../datatypes";
import {ProductIterator} from "../ProductIterator";
import {LinearRangeIterator} from "../LinearRangeIterator";

describe("ProductIterator", () => {
    it("can iterate forward", () => {
        var map:any = {};
        map.test1 = new LinearRangeIterator(new ImmutableRange(0, 4), 2);
        map.test2 = new LinearRangeIterator(new ImmutableRange(0, 1), 0.5);
        var iter = new ProductIterator(map);
        var iter1Val = iter.next();
        chai.expect(iter1Val.test1).to.equal(0);
        chai.expect(iter1Val.test2).to.equal(0);
        iter1Val = iter.next();
        chai.expect(iter1Val.test1).to.equal(2);
        chai.expect(iter1Val.test2).to.equal(0);
        iter1Val = iter.next();
        chai.expect(iter1Val.test1).to.equal(4);
        chai.expect(iter1Val.test2).to.equal(0);
        iter1Val = iter.next();
        chai.expect(iter1Val.test1).to.equal(0);
        chai.expect(iter1Val.test2).to.equal(0.5);
        iter1Val = iter.next();
        chai.expect(iter1Val.test1).to.equal(2);
        chai.expect(iter1Val.test2).to.equal(0.5);
        iter1Val = iter.next();
        chai.expect(iter1Val.test1).to.equal(4);
        chai.expect(iter1Val.test2).to.equal(0.5);
        iter1Val = iter.next();
        chai.expect(iter1Val.test1).to.equal(0);
        chai.expect(iter1Val.test2).to.equal(1);
        iter1Val = iter.next();
        chai.expect(iter1Val.test1).to.equal(2);
        chai.expect(iter1Val.test2).to.equal(1);
        iter1Val = iter.next();
        chai.expect(iter1Val.test1).to.equal(4);
        chai.expect(iter1Val.test2).to.equal(1);
        iter1Val = iter.next();
        chai.expect(iter1Val).to.equal(null);
    });
});