/// <reference path="../../types/types.d.ts" />

import * as chai from "chai";
import * as fs from "fs";
import * as path from "path";
import {LinearSvm} from "../LinearSvm";
import {BinaryLabel, Example} from "../datatypes";
import {RbfKernel, LinearKernel, PolynomialKernel} from "../kernels";
import {Svm} from "../Svm";
import {SvmLightParser} from "../SvmLightParser";
import {PartitionedDataSet} from "../PartitionedDataSet";
import * as JSONSerializer from "../JSONSerializer";
import * as SvmFactory from "../SvmFactory";

describe("Svm", function () {
	it("can construct a Linear SVM", () => {
		var svmObj = new LinearSvm();
	});

	it("can train a Linear SVM", () => {
		var label = BinaryLabel;
		var DataPoint = Example;
		var data:Array<Example> = [];
		data.push(new DataPoint(new Float64Array([4, 4, 6, 8]), label.UP));
		data.push(new DataPoint(new Float64Array([4, 23, 6, 8]), label.UP));
		data.push(new DataPoint(new Float64Array([5, 4, 6, 8]), label.UP));
		data.push(new DataPoint(new Float64Array([6, 4, 6, 8]), label.UP));
		data.push(new DataPoint(new Float64Array([1, 4, 6, 8]), label.DOWN));
		data.push(new DataPoint(new Float64Array([2, 4, 6, 8]), label.DOWN));
		data.push(new DataPoint(new Float64Array([3, 4, 6, 8]), label.DOWN));

		var svmObj = new LinearSvm();
		svmObj.fit(data);
		console.log("Training metrics", svmObj.trainingMetrics);
		console.log("Error cache metrics", svmObj.errorCacheMetrics);
		console.log("Kernel cache metrics", svmObj.kernelCacheMetrics);
		chai.expect(svmObj.trained).to.equal(true);
		chai.expect(svmObj.classify(new Float64Array([1, 4, 5, 7]))).to.be.equal(label.DOWN);
		chai.expect(svmObj.classify(new Float64Array([2, 4, 6, 8]))).to.be.equal(label.DOWN);
		chai.expect(svmObj.classify(new Float64Array([5, 23, 25, 27]))).to.be.equal(label.UP);
	});

	it("can train an RBF SVM", () => {
		var label = BinaryLabel;
		var DataPoint = Example;
		var data:Array<Example> = [];
		data.push(new DataPoint(new Float64Array([4, 4, 6, 8]), label.UP));
		data.push(new DataPoint(new Float64Array([4, 23, 6, 8]), label.UP));
		data.push(new DataPoint(new Float64Array([5, 4, 6, 8]), label.UP));
		data.push(new DataPoint(new Float64Array([6, 4, 6, 8]), label.UP));
		data.push(new DataPoint(new Float64Array([1, 4, 6, 8]), label.DOWN));
		data.push(new DataPoint(new Float64Array([2, 4, 6, 8]), label.DOWN));
		data.push(new DataPoint(new Float64Array([3, 4, 6, 8]), label.DOWN));
		var kernel = new RbfKernel(1);
		var svmObj = new Svm(kernel);
		svmObj.fit(data);
		console.log("Training metrics", svmObj.trainingMetrics);
		console.log("Error cache metrics", svmObj.errorCacheMetrics);
		console.log("Kernel cache metrics", svmObj.kernelCacheMetrics);
		chai.expect(svmObj.trained).to.equal(true);
		chai.expect(svmObj.classify(new Float64Array([1, 4, 5, 7]))).to.be.equal(label.DOWN);
		chai.expect(svmObj.classify(new Float64Array([2, 4, 6, 8]))).to.be.equal(label.DOWN);
		chai.expect(svmObj.classify(new Float64Array([5, 23, 25, 27]))).to.be.equal(label.UP);
	});

	it("can be serialized", () => {
		var label = BinaryLabel;
		var DataPoint = Example;
		var data:Array<Example> = [];
		data.push(new DataPoint(new Float64Array([4, 4, 6, 8]), label.UP));
		data.push(new DataPoint(new Float64Array([4, 23, 6, 8]), label.UP));
		data.push(new DataPoint(new Float64Array([5, 4, 6, 8]), label.UP));
		data.push(new DataPoint(new Float64Array([6, 4, 6, 8]), label.UP));
		data.push(new DataPoint(new Float64Array([1, 4, 6, 8]), label.DOWN));
		data.push(new DataPoint(new Float64Array([2, 4, 6, 8]), label.DOWN));
		data.push(new DataPoint(new Float64Array([3, 4, 6, 8]), label.DOWN));

		var svmObj = new Svm(new LinearKernel());
		svmObj.fit(data);
		var str:string = JSONSerializer.toJSON(svmObj);
		var json:any = JSON.parse(str);
		chai.expect(json.kernel._type).to.equal("nlearn.LinearKernel");
		var svmObj2 = JSONSerializer.fromJSON(str);
		chai.expect(svmObj2.classify(new Float64Array([1, 4, 5, 7]))).to.be.equal(label.DOWN);
	});

	it("can be serialized (linear)", () => {
		var label = BinaryLabel;
		var DataPoint = Example;
		var data:Array<Example> = [];
		data.push(new DataPoint(new Float64Array([4, 4, 6, 8]), label.UP));
		data.push(new DataPoint(new Float64Array([4, 23, 6, 8]), label.UP));
		data.push(new DataPoint(new Float64Array([5, 4, 6, 8]), label.UP));
		data.push(new DataPoint(new Float64Array([6, 4, 6, 8]), label.UP));
		data.push(new DataPoint(new Float64Array([1, 4, 6, 8]), label.DOWN));
		data.push(new DataPoint(new Float64Array([2, 4, 6, 8]), label.DOWN));
		data.push(new DataPoint(new Float64Array([3, 4, 6, 8]), label.DOWN));

		var svmObj = new LinearSvm();
		svmObj.fit(data);
		var str:string = JSONSerializer.toJSON(svmObj);
		var json:any = JSON.parse(str);
		chai.expect(json.kernel._type).to.equal("nlearn.LinearKernel");
		var svmObj2 = JSONSerializer.fromJSON(str);
		chai.expect(svmObj2.classify(new Float64Array([1, 4, 5, 7]))).to.be.equal(label.DOWN);
	});

	it("can serialize training data", () => {
		var data = new Array<Example>();
		var label = BinaryLabel;
		var DataPoint = Example;
		data.push(new DataPoint(new Float64Array([4, 4, 6, 8]), label.UP));
		data.push(new DataPoint(new Float64Array([4, 23, 6, 8]), label.UP));
		data.push(new DataPoint(new Float64Array([5, 4, 6, 8]), label.UP));
		data.push(new DataPoint(new Float64Array([6, 4, 6, 8]), label.UP));
		data.push(new DataPoint(new Float64Array([1, 4, 6, 8]), label.DOWN));
		data.push(new DataPoint(new Float64Array([2, 4, 6, 8]), label.DOWN));
		data.push(new DataPoint(new Float64Array([3, 4, 6, 8]), label.DOWN));
		var str:string = JSONSerializer.toJSON(data);
		var data2:Array<Example> = JSONSerializer.fromJSON(str);
		chai.expect(data2[0].features[0]).to.equal(4);
	});

	it("can train a linear svm on test dataset", function (cb) {
		this.timeout(10000);
		var file = fs.createReadStream(path.resolve(__dirname, "../../test-data/svmlight.dat"));
		var parser:SvmLightParser = new SvmLightParser(file, 7974);
		var promise = parser.parse();
		promise.then((ds:Array<Example>):void => {
			var pds = new PartitionedDataSet(ds, .1);
			var s:Svm = SvmFactory.create(new LinearKernel());
			s.fit(pds.trainingSet);
			var accuracy:number = s.evaluate(pds.evaluationSet);
			chai.expect(accuracy).to.be.greaterThan(0.90);
			cb();
		}).done();
	});

	it("can train with a Polynomial Kernel", function () {
		var label = BinaryLabel;
		var DataPoint = Example;
		var data:Array<Example> = [];
		data.push(new DataPoint(new Float64Array([4, 4, 6, 8]), label.UP));
		data.push(new DataPoint(new Float64Array([4, 23, 6, 8]), label.UP));
		data.push(new DataPoint(new Float64Array([5, 4, 6, 8]), label.UP));
		data.push(new DataPoint(new Float64Array([6, 4, 6, 8]), label.UP));
		data.push(new DataPoint(new Float64Array([1, 4, 6, 8]), label.DOWN));
		data.push(new DataPoint(new Float64Array([2, 4, 6, 8]), label.DOWN));
		data.push(new DataPoint(new Float64Array([3, 4, 6, 8]), label.DOWN));
		var kernel = new PolynomialKernel(2, 1);
		var svmObj = new Svm(kernel);
		svmObj.fit(data);
		console.log("Training metrics", svmObj.trainingMetrics);
		console.log("Error cache metrics", svmObj.errorCacheMetrics);
		console.log("Kernel cache metrics", svmObj.kernelCacheMetrics);
		chai.expect(svmObj.trained).to.equal(true);
		chai.expect(svmObj.classify(new Float64Array([1, 4, 5, 7]))).to.be.equal(label.DOWN);
		chai.expect(svmObj.classify(new Float64Array([2, 4, 6, 8]))).to.be.equal(label.DOWN);
	});
});