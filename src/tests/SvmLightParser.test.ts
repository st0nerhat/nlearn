/// <reference path="../../types/types.d.ts" />

import * as chai from "chai";
import * as fs from "fs";
import * as path from "path";
import {BinaryLabel, Example} from "../datatypes";
import {SvmLightParser} from "../SvmLightParser";

describe("SvmLightParser", () => {
	it("can parse an SVM Light dataset", (cb) => {
		var file = fs.createReadStream(path.resolve(__dirname, "../../test-data/svmlight.dat"));

		var parser:SvmLightParser = new SvmLightParser(file, 7974);
		var promise = parser.parse();
		promise.then((ds:Array<Example>):void => {
			chai.expect(ds[0].label).to.equal(BinaryLabel.UP);
			chai.expect(ds[0].features[6]).to.equal(0.0342598670723747);
			chai.expect(ds[516].label).to.equal(BinaryLabel.DOWN);
			cb();
		}).done();
	});
});