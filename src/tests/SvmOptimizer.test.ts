/// <reference path="../../types/types.d.ts" />

import * as chai from "chai";
import {BinaryLabel, Example, ImmutableRange} from "../datatypes";
import {RbfKernel, LinearKernel} from "../kernels";
import {GeometricRangeIterator} from "../GeometricRangeIterator";
import {ArrayIterator} from "../ArrayIterator";
import {SvmOptimizer} from "../SvmOptimizer";

describe("SvmOptimizer", () => {
	it("can choose an optimal SVM", function () {
		var tolerance = new GeometricRangeIterator(new ImmutableRange(1e-10, 1e-3), 10);
		var alphaTolerance = new GeometricRangeIterator(new ImmutableRange(1e-10, 1e-3), 10);
		var C = new GeometricRangeIterator(new ImmutableRange(0.1, 1000), 10);
		var kernels = [new LinearKernel(), new RbfKernel(1), new RbfKernel(10)];
		var kernelIterator = new ArrayIterator(kernels);
		var optimizer = new SvmOptimizer(kernelIterator, tolerance, alphaTolerance, C);

		var label = BinaryLabel;
		var DataPoint = Example;
		var data:Array<Example> = [];
		data.push(new DataPoint(new Float64Array([4, 4, 6, 8]), label.UP));
		data.push(new DataPoint(new Float64Array([5, 4, 6, 8]), label.UP));
		data.push(new DataPoint(new Float64Array([6, 4, 6, 8]), label.UP));
		data.push(new DataPoint(new Float64Array([1, 4, 6, 8]), label.DOWN));
		data.push(new DataPoint(new Float64Array([2, 4, 6, 8]), label.DOWN));
		data.push(new DataPoint(new Float64Array([3, 4, 6, 8]), label.DOWN));
		var result = optimizer.optimize(data);
		console.log("Optimizer metrics", result.metrics);
	});
});